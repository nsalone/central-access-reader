//
//  ___ ____  __  ____ ____ ___ ____ ____ ___ ___ 
// / __)_  _)/__\(_  _)_  _) __)_  _)_  _) __) __)
// \__ \ )( /(__)\ )(  _)(_\__ \ )(  _)(( (__\__ \
// (___/(__)__)(__)__)(____)___/(__)(____)___)___/
//
// --------------
// Order Matters!
// --------------
//
// The MathML parser is entirely sequential. Once it finds all cases of the
// current pattern and combines the different pattern's elements into singular
// entities, the pattern is no longer used in later matching. This means you
// must order patterns correctly to produce results you expect.
//
// -----------------
// Expected Ordering
// -----------------
//
// In order for patterns to match correctly, you should have patterns grouped
// and have those groups ordered in the following way: 
//
// 1. Symbol Code (like variables and Unicode)
// 2. Fraction-Based (like derivatives)
// 3. Over-Under (like sums and integrals)
// 4. Parenthetical Expressions (like square roots or things in parenthesis)
// 5. Generic Fallbacks (like fractions, superscripting, and subscripting)
//
// ------------------------------------------------------------------------------
// Symbol Code
// ------------------------------------------------------------------------------
import _codes.txt
import _Numbers.txt

// Unicodes and MathML codes for operators
import _Unicodes.txt
import _Negatives.txt

e [variable] = mi("e") -> '"e"'
i [variable] = mi("i") -> '"i"'
z [variable] = mi("z") -> '"z"'

bbcapR = mi("&#8477;") -> 'all real numbers'

// Other symbols

// Strikes and Slashes
import _Vocab.txt
import _BordersSlashes.txt

//---------------------------------------------------------------
// Trigonometry-Specific
//---------------------------------------------------------------
import _Trig.txt

// Products and Unions
import _Integrals.txt

//Powers and Primes
apostrophe = apostrophe -> 'prime'
import _FunctionsLimits.txt


// Syntax Quirks
doubleBarSyn = mover(mover(mrow(+) bar) bar) -> '{1} under double bar'
doubleBarSyn = munder(munder(mrow(+) bar) bar) -> '{1} over double bar'
doubleDagger = dagger dagger -> 'double dagger'

// ------------------------------------------------------------------------------
// Fraction-based
// ------------------------------------------------------------------------------
integral = msub([integral] ?) -> '{1} sub {2} of,'

// Other symbols
infinity = mi("&#8734;") -> 'infinity'
infinity = mn("&#8734;") -> 'infinity'

// ------------------------------------------------------------------------------
// Conditions for arrows based on over or under
// ------------------------------------------------------------------------------
overBothArrow = munder(? [arrow]) -> '{1}, over {2},'
underBothArrow = mover(? [arrow]) -> '{1}, under {2},'
overUnderArrowB = munderover(rightArrow mrow(+) mrow(+)) -> '{2}, above {1}, sub {3},'
overUnderArrowB = munderover(leftArrow mrow(+) mrow(+)) -> '{2}, above {1}, sub {3},'
overUnderArrowB = munderover(bothArrow mrow(+) mrow(+)) -> '{2}, above {1}, sub {3},'
overUnderArrowB = munderover(rightHarpoon mrow(+) mrow(+)) -> '{2}, above {1}, sub {3},'

// ------------------------------------------------------------------------------
// Statistics and Finance
// ------------------------------------------------------------------------------
chiSquare = msup( chi mrow(two)) -> 'chi-square,'
chiSquare = msup( chi two) -> 'chi-square,'
chiSquare = msup( Chi mrow(two)) -> 'chi-square,'
chiSquare = msup( Chi two) -> 'chi-square,'
degreesFreedom = d f -> 'degrees of freedom'
degreesFreedom = d point f point -> 'degrees of freedom'
probableError = P E -> 'probable error'
probableError = P point E point -> 'probable error'
standardDev = s d -> 'standard deviation'
standardDev = s point d point -> 'standard deviation'

// ------------------------------------------------------------------------------
// Under-Over, Integrals, Products, and Summations
// ------------------------------------------------------------------------------
backPrimes = mmultiscripts(? ? ? ?) -> '{4} {1}'

// Limits
import _JustLimits.txt

// ------------------------------------------------------------------------------
// Parenthetical Expressions
// ------------------------------------------------------------------------------
absoluteValue = verticalBar + verticalBar -> 'absolute value {2} end absolute'
import _Roots.txt

// ------------------------------------------------------------------------------
// Matrices
// ------------------------------------------------------------------------------

// ------------------------------------------------------------------------------
// Generic Fallbacks, Final Collectors
// ------------------------------------------------------------------------------
import _SoftFractions.txt
import _Dollars.txt
import _Fractions.txt
import _Fences.txt
import _Final.txt